import { Component } from '@angular/core';
import {DataStorageService} from '../shared/data-storage.service';
import { Response } from '@angular/http';
import {Recipe} from '../recipes/recipe.model';
import {RecipeService} from '../recipes/recipe.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  constructor(private dataStorageService: DataStorageService,
              private recipeService: RecipeService) {

  }
  onSave() {
    this.dataStorageService.storeRecipes().subscribe(
      (response: Response) => {console.log(response); }
    ) ;
  }
  onFetch() {
    this.dataStorageService.fetchRecipes().subscribe(
      (response: Response) => {
        const recipes: Recipe[] = response.json();
        this.recipeService.setRecipes(recipes);
      }
    );
  }
}
